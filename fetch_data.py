# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

#%%
import musicbrainzngs
from owlready2 import *
import os
from datetime import date
import pathlib
import nltk
from nltk.corpus import wordnet as wn

musicbrainzngs.set_useragent('musicbrainz-modsem','0.1')


def look_for_artists(artist, artist_type):
    
    return musicbrainzngs.search_artists(artist= artist, type=artist_type)['artist-list']
    # for artist in result['artist-list']:
    #     print(u"{id}: {name}".format(id=artist['id'], name=artist["name"]))

def get_artist(artist_id, includes = [], release_type= []):
    try:
        result = musicbrainzngs.get_artist_by_id(artist_id, includes, release_type = release_type)
    except musicbrainzngs.ResponseError as exc:
        print("Something went wrong with the request: %s" % exc)
    else:
        artist = result["artist"]
        # print("name:\t\t%s" % artist["name"])
        # print("sort name:\t%s" % artist["sort-name"])
        return result 
        

def pretty_print(releases, get='all'):
    if type(releases) is not list:
        rels = [releases]
    else:
        rels = releases
        
    for release in rels:
        if get=='all':
            for info in release.keys():
                print(f"{info}: {release[info]}")
        elif get in release.keys():
            print(f"{get}: {release[get]}")
        print('####################################')
    
def get_albums(artist, includes = []):
    result = musicbrainzngs.get_artist_by_id(artist, includes=["release-groups"], release_type=["album","ep"])
    # print("Keys: {}".format(result['artist'].keys()))
    albums = {}
    for release_group in result["artist"]["release-group-list"]:
        if release_group['type'] == 'Album':
            albums[release_group['id']] = release_group['title'] 
            # albums.append(release_group) 
        # print("{title} ({type})".format(title=release_group["title"],type=release_group["type"]))
    return albums

        
def get_album_songs(album):
    data =  musicbrainzngs.get_release_group_by_id(album, includes =['releases'])
    rels = data['release-group']['release-list']
    fetched_rels = []
    for rel in rels:
        fetched_rels.append(musicbrainzngs.get_release_by_id(rel.get('id'), includes=['recordings']).get('release'))
    
    tracks = {}

    fetched_rels = [fetched_rels[0]]
    album_date = fetched_rels[0]['release-event-list'][0]['date']
    formation = find_formation_by_period(onto, album_date).hasFormationMember
    authors = {}
    for (j,rel) in enumerate(fetched_rels):
        medium_list = rel.get('medium-list')
        medium = medium_list[0]
        track_list = medium.get('track-list')
        for (i,track) in enumerate(track_list):
            recording = track.get('recording')
            fetched_recording = musicbrainzngs.get_recording_by_id(recording['id'], includes = ['work-rels'])
            related_work = fetched_recording['recording']['work-relation-list'][0]['work']          
            tracks[related_work['id']] = related_work['title']
            authors[related_work['title']] = musicbrainzngs.get_work_by_id(related_work['id'], includes = ['artist-rels','work-rels'])['work'].get('artist-relation-list')
    return (album_date, tracks, authors)

def get_band_members(band_id):
    rels = musicbrainzngs.get_artist_by_id(band_id, includes=['artist-rels'], release_status=[], release_type=[])['artist']['artist-relation-list']
    band_members = []
    for rel in rels:
        if rel.get('type') == 'member of band':
            member = {}
            member['id'] = rel['artist']['id']
            member['nome'] = rel['artist']['name']
            member['inizio'] = rel['begin']
            if 'end' in rel.keys():
                member['fine'] = rel['end']
            if 'ended' in rel.keys():
                member['ended'] = rel['ended']
            member['attributi'] = rel['attribute-list']
            band_members.append(member)
    return band_members

def get_events_performances(artist):
    rels = musicbrainzngs.get_artist_by_id(queen_id, includes=['event-rels'], release_status=[], release_type=[])['artist']['event-relation-list']
    events = []
    for rel in rels:
        events.append(rel['event'])
    return events

def to_date(string):
    if string is None:
        return None
    splitted = string.split('-')
    year = 0
    month = 1
    day = 1
    if len(splitted) == 1:
        year = splitted[0]
    if len(splitted) == 2:
        year,month = splitted
    if len(splitted) == 3:
        year, month, day = splitted
    return date(int(year), int(month), int(day))
    
def create_formations(band):
    members = sorted(get_band_members(band), reverse = True ,key = lambda k: k['inizio'])
    formations = []
    results = []   

    starting_formation = []
    for m in members:
        if 'original' in m['attributi']:
            starting_formation.append(m)
        else:
            formations.append([m])
    formations.append(starting_formation)
    for m in members:
        for formation in formations:
            if (not is_member(m, formation)) and in_formation(m, formation):
                formation.append(m)
                
    for (i,f) in enumerate(formations):
        results.append({})
        results[i]['members'] = f
        start, end = compute_start_end_formation(f)
        results[i]['start'] = start
        results[i]['end'] = end
                
    return results

        
def compute_start_end_formation(formation):
    start = formation[0]['inizio']
    for member in formation:
        if 'fine' in member.keys():
            end = member['fine']
            break

    for member in formation:
        if member['inizio'] > start:
            start = member['inizio']
        if 'fine' in member.keys():
            if member['fine'] < end:
                end = member['fine']
    return (start, end)

def is_member(x, formation):
    for member in formation:
        if x['id'] == member['id']:
            return True
    return False



def in_formation(x, formation):
    start_before_end = True
    start_before_start = True
    end_before_start = False
    for f in formation:
        if x['id'] != f['id'] and 'fine' in f.keys():
            if 'fine' in x.keys():
                end_before_start = to_date(x['fine']) <= to_date(f['inizio']) 
            start_before_end = start_before_end and (to_date(x['inizio']) < to_date(f['fine']) and (not end_before_start) and (not to_date(x['inizio']) > to_date(f['inizio'])))
            start_before_start = start_before_start and (to_date(x['inizio']) < to_date(f['inizio']) and (not end_before_start))
    return start_before_end or start_before_start
    
def path2url(path):
    """Return file:// URL from a filename."""
    path = os.path.abspath(path)
    if isinstance(path, unicode):
        path = path.encode('utf8')
    return 'file:' + urlparse.pathname2url(path)

##############################################################################
def insert_band(onto, band):
    with onto:        
        classes = list(onto.classes())
        for c in classes:
            if c._name == 'Gruppo':
                _band = c(band)
                _band.foaf_name = [band]
                break

def compute_role(onto, member):
    attributes = member['attributi']
    res = []
    for attr in attributes:
        if attr == 'lead vocals':
            res.append(look_for_class(onto, 'RuoloPrimaVoce'))
        elif attr == 'background vocals':
           res.append(look_for_class(onto, 'RuoloVoceSecondaria')) 
        if attr == 'bass guitar':
            res.append(look_for_class(onto, 'RuoloBassista'))
        if attr == 'guitar':
            res.append(look_for_class(onto, 'RuoloChitarrista'))
        if 'drum set' in attr:
            res.append(look_for_class(onto, 'RuoloBatterista'))
    return res
        
def insert_formations_and_members(onto, band_name, formations):
    formation_class = None
    musician_class = None
    sorted_formations = sorted(formations, key=lambda k: k['start'])
    band_individual = look_for_individual(onto, band_name,"Gruppo")
    print(to_date(sorted_formations[0]['start']))
    band_individual.startArtistActivity = get_time_instant(onto, sorted_formations[0]['start'])
    band_individual.endArtistActivity = get_time_instant(onto, sorted_formations[0]['end'])
    with onto:
        musician_class = look_for_class(onto, 'Musicista')
        for i,f in enumerate(sorted_formations):
            # print(f"{band_name} ({str(f['start'])},{str(f['end'])}")
            if i > 0:
                formation_name = generate_individual_name('Formazione', band_name = band_name, formation = f)
                formation_class = look_for_class(onto, 'Formazione')
            else:
                formation_name = generate_individual_name('FormazioneStorica', band_name = band_name, formation = f)
                formation_class = look_for_class(onto, 'FormazioneStorica')
            formation_individual = formation_class(formation_name)
            formation_individual.label = [formation_name]
            formation_individual.startActivity = get_time_instant(onto, f['start'])
            formation_individual.endActivity = get_time_instant(onto, f['end'])
            band_individual.hadFormation.append(formation_individual)
            for member in f['members']:
                member_individual = musician_class(generate_individual_name('Musicista', musician_name = member['nome']))
                member_individual.foaf_name = [member['nome']]
                formation_individual.hasFormationMember.append(member_individual)
                roles = compute_role(onto,member)
                for role in roles:
                    role_name = generate_individual_name('Ruolo', role = role, band_name = band_name, formation = f)
                    r = role(role_name)
                    r.label = [role_name]
                    member_individual.hasRole.append(r)
                    r.roleInFormation.append(formation_individual)

def look_for_individual(onto, individual_name, class_name):
    cl = look_for_class(onto, class_name)
    for ind in onto.individuals():
        if ind._name.lower() == individual_name.lower() and cl in ind.is_a:
            return ind
    return None

def look_for_class(onto, class_name):
    imported_ontos = onto.imported_ontologies
    for class_ in onto.classes():
        if class_._name == class_name:
            return class_

    for _onto in imported_ontos:
        for _class in _onto.classes():
            if _class._name == class_name:
                return _class
    return None

def get_individuals_by_class(onto, class_name):
    individuals = list(onto.individuals())
    
    for imported in onto.imported_ontologies:
        individuals += list(imported.individuals())

    res = []
    for individual in individuals:
        if is_a(individual, class_name):
            res.append(individual)
    return res

def get_individual(onto, individual):
    classes = list(onto.classes())
    for imported in onto.imported_ontologies:
        classes += list(imported.classes())

    for _class in classes:
        individuals = get_individuals_by_class(onto, _class)
        for ind in individuals:
            if ind._name == individual or ind == individual:
                return ind
    return None


def is_a(x, _class):
    ancestors = [ancestor._name for cl in x.is_a for ancestor in cl.ancestors()]
    _class_ = ''
    if isinstance(_class, str):
        _class_ = _class
    else:
        _class_ = _class._name
    for cl in x.is_a:
        if cl._name == _class_ or _class_ in ancestors:
            return True
    return False

def insert_albums_songs(onto, band_id, band_name):
    print("Fetching albums...")
    albums = get_albums(band_id)
    for album_id in albums.keys():
        album_name = albums[album_id]
        insert_single_album_songs(onto, album_id, album_name, band_id, band_name)

def insert_single_album_songs(onto, album_id, album_name, band_id, band_name):
    with onto:
        # if look_for_individual(onto, album_name, "Album") is None:
            new_album = look_for_class(onto,'Album')(album_name)
            new_album.title = [album_name]
            album_date, tracklist, authors = get_album_songs(album_id)
            band_ind = get_individual(onto, band_name)
            release_event = look_for_class(onto, 'UscitaAlbum')(generate_individual_name('UscitaAlbum', album_name = album_name))
            release_event.involvedAgent = [band_ind]
            release_event.involved = [new_album]
            release_event.atTime = get_time_instant(onto, album_date)
            creator_formation = [x.name for x in find_formation_by_period(onto, album_date).hasFormationMember]
            print(creator_formation)
            last_item = None
            for (i,track_id) in enumerate(tracklist.keys()):
                track_name = tracklist[track_id]
                track_class = look_for_class(onto, 'BranoMusicale')
                new_track = track_class(generate_individual_name('BranoMusicale',track_name = track_name, album_name = album_name))
                musical_work = get_work(onto, track_name)
                musical_work.title = [track_name]
                new_track.realizationOf = musical_work
                band_ind.creatorOf.append(musical_work)
                new_track.title = [track_name]
                new_item = look_for_class(onto, 'ListItem')(f"Item{album_name}{track_name}")
                new_item.itemContent.append(new_track)
                if i == 0:
                    new_item.firstItemOf.append(new_album)
                elif i == len(tracklist.keys())-1:
                    new_item.lastItemOf.append(new_album)
                    new_item.previousItem = last_item
                else:
                    new_item.previousItem = last_item
                new_item.itemContent = [new_track]
                last_item = new_item
                
                collaborators = []
                for auth in authors[track_name]:
                    auth_name = ''.join(auth['artist']['name'].split())
                    if auth_name not in creator_formation:
                        collaborator = get_artist(onto, auth_name)
                        collaborator.creatorOf.append(musical_work)
                    

def get_work(onto, track):
    work_name = generate_individual_name('MusicalWork', track_name = track)
    work_ind = get_individual(onto, work_name)
    with onto:
        if work_ind is None:
            new_work = look_for_class(onto, 'MusicalWork')(work_name)
            return new_work
        else:
            return work_ind
        
def get_artist(onto, artist):
    artist_name = generate_individual_name('Musicista',musician_name = artist)
    artist_ind = get_individual(onto, artist_name)
    if artist_ind is None:
        artist_ind = look_for_class(onto, 'Musicista')(artist_name)
        artist_ind.name = artist
    return artist_ind


def compute_events(onto, band_id, band_name, formations):
    sorted_formations = sorted(formations, key=lambda k: k['end'])
    events = []
    event_class = look_for_class(onto,'Evento')
    event_leave_class = look_for_class(onto,'AbbandonoMembro')
    event_new_class = look_for_class(onto,'NuovoMembro')
    event_death_class = look_for_class(onto,'EventoMorte')
    event_create_class = look_for_class(onto,'CreazioneGruppo')
    this_event = None
    
    #Cerchiamo l'evento di creazione della band
    min_time = min([formation['start'] for formation in sorted_formations])
    for i,formation in enumerate(sorted_formations):
        if formation['start'] == min_time:
            events.append({'event': event_create_class(f"Creazione{band_name}"), 'time': to_date(formation['start'])})
            del sorted_formations[i]
            break
    # print("Formazioni:",[(f['start'],f['end']) for f in sorted_formations])
    print("*** inserting events...")
    for (i, formation) in enumerate(sorted_formations):
        for member in formation['members']:
            artist = look_for_artists(member['nome'], 'Person')[0]
            member_individual = get_individual(onto, generate_individual_name(musician_name = member['nome'], class_name = 'Musicista'))
            formation_individual = get_individual(onto, generate_individual_name('Formazione', band_name = band_name, formation = formation))
            # print(member['nome'],member.get('inizio'), member.get('fine'),"|||", formation['start'],formation['end'])
            
            if len(events) > 0:
                events = sorted(events, key=lambda k: k['time'])
                
            with onto:
                if artist['life-span'].get('end') is not None:
                    if to_date(artist['life-span']['end']) == to_date(formation['end']):
                        this_event = (event_death_class(generate_individual_name(class_name = 'MorteMembro', musician_name=member['nome'])))
                        this_event.involvedAgent.append(member_individual)
                        this_event.involved.append(formation_individual)
                        events.append({'event':this_event,'time':to_date(formation['end'])})
                        time = get_time_instant(onto, artist['life-span']['end'])
                        this_event.atTime = time
                       
                if to_date(member.get('inizio')) == to_date(formation['start']):
                    this_event = event_new_class(generate_individual_name(class_name = 'NuovoMembro', musician_name=member['nome']))
                    if len(events) > 1:
                        if is_a(events[len(events)-1]['event'], 'AbbandonoMembro'):
                            events[len(events)-1]['event'].involved.append(formation_individual)
                   
                    last_formation = get_individual(onto,generate_individual_name('Formazione',band_name = band_name, formation = sorted_formations[i-1]))
                    this_event.involved.append(last_formation)
                    this_event.involved.append(formation_individual)
                    this_event.involvedAgent.append(member_individual)
                    events.append({'event':this_event, 'time':to_date(formation['start'])})
                    time = get_time_instant(onto, member.get('inizio'))
                    this_event.atTime = time
                
                if to_date(member.get('fine')) == to_date(formation['end']) and to_date(artist['life-span'].get('end')) != to_date(formation['end']):
                    this_event = event_leave_class(generate_individual_name(class_name = 'AbbandonoMembro', musician_name=member['nome']))
                    this_event.involvedAgent.append(member_individual)
                    this_event.involved.append(formation_individual)
                    events.append({'event': this_event, 'time':to_date(formation['end'])})
                    time = get_time_instant(onto, member.get('fine'))
                    this_event.atTime = time
                    
    print("*** inserting super events...")    
    k = 1
    added_events = []
    for i in range(0, len(events)):
        for j in range(1,len(events)):
            if events[i]['time'] == events[j]['time'] and events[i]['event'] != events[j]['event']:
                added = False   
                for e1, e2 in added_events:
                    if (e1 == events[i]['event'] and e2 == events[j]['event']) or (e1 == events[j]['event'] and e2 == events[i]['event']):
                        added = True
                        break
                if added == False:
                    super_event = event_class(f"ModificaFormazione({band_name}{k})")
                    events[i]['event'].subEventOf.append(super_event)
                    events[j]['event'].subEventOf.append(super_event)
                    added_events.append((events[i]['event'],events[j]['event']))
                    k+=1
                

def generate_individual_name(class_name, album_name = '', track_name = '' , role = None, musician_name = '', band_name = '', formation = None):
    if class_name == 'Formazione' or class_name == 'FormazioneStorica':
        return f"{band_name}({str(formation['start'])},{str(formation['end'])})"
    elif class_name == 'Musicista':
        return ''.join(musician_name.split())
    elif class_name == 'AbbandonoMembro':
        return f"Abbandono{''.join(musician_name.split())}"
    elif class_name == 'NuovoMembro':
        return f"Arrivo{''.join(musician_name.split())}"
    elif class_name == 'MorteMembro':
        return f"Morte{''.join(musician_name.split())}"
    elif class_name == 'CreazioneGruppo':
        return f"Creazione{band_name}"
    elif class_name == 'Ruolo':
        formation_name = f"{band_name}({str(formation['start'])},{str(formation['end'])})"
        return f"{role._name}{formation_name}"
    elif class_name == 'BranoMusicale':
        return f"{''.join(track_name.split())}{album_name}"
    elif class_name == 'MusicalWork':
        return ''.join(track_name.split())
    elif class_name == 'UscitaAlbum':
        return f"UscitaAlbum{album_name}"
    else:
        return None

def get_time_instant(onto, instant):
    instant_ind = look_for_individual(onto, instant, 'Instant')
    instant_class = look_for_class(onto, 'Instant')
    if instant_ind is None:
        with onto:
            new_instant = instant_class(instant)
            new_instant.inXSDDate = to_date(instant)
            return new_instant    
    return instant_ind
        

def find_formation_by_period(onto, instant):
    formations = get_individuals_by_class(onto, 'Formazione')
    sorted_formations = sorted(formations, reverse = True,key = lambda k: k.startActivity.inXSDDate)
    for formation in sorted_formations:
        if to_date(instant) >= formation.startActivity.inXSDDate and to_date(instant) <= formation.endActivity.inXSDDate:
            return formation
    return None

def path_to_uri(path, file_name):
    uri = pathlib.Path(path + "/" + file_name).as_uri() 
    return uri

path = os.path.dirname(os.path.realpath(__file__))


file_uri = path_to_uri(path,'musicstory.owl')
file_uri = file_uri.replace('/','',1)
corrected_path = path.replace("\\", "/")

onto_path.append(corrected_path)
onto = get_ontology(file_uri)
onto.load()

#Si cambia il nome di foaf:name
for prop in list(onto.properties()):
    if prop.python_name == 'name':
        prop.python_name = 'foaf_name'

band = 'Queen'
band_name = "".join(band.split())
insert_band(onto, band_name)

band_id = look_for_artists(band_name, 'group')[0]['id']
print("Computing formations...")
formations = create_formations(band_id)
print("Inserting computed formations...")
insert_formations_and_members(onto, band_name, formations)
print("Computing events...")
compute_events(onto, '',band_name, formations)
print("Inserting albums and songs...")
insert_single_album_songs(onto, '3918b90b-340e-3779-9d7e-ba1593653498', 'HotSpace', band_id, band_name)

onto.save()
# print(get_individual(onto, band_name))
# print("Fetching albums...")
# album_date, tracks, authors = get_album_songs('3918b90b-340e-3779-9d7e-ba1593653498')
# print(authors)


# pretty_print(get_band_members(queen_id))

# pretty_print(get_events_performances(queen_id))
# pretty_print(get_album_songs('3918b90b-340e-3779-9d7e-ba1593653498'))

        

